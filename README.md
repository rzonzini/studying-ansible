## Build das imagens

1. Entre no diretório com os manifestos da imagem
    <pre>
    <code>
    cd ./Docker
    </code>
    </pre>

2. Cria a imagem
    <pre>
    <code>
    docker build -t &lt;<i>REGISTRY</i>&gt;/&lt;<i>NOME_DA_IMAGEM</i>&gt;:&lt;<i>TAG_DA_IMAGEM</i>&gt; -t &lt;<i>REGISTRY</i>&gt;/&lt;<i>NOME_DA_IMAGEM</i>&gt;:&lt;<i>OUTRA_TAG_DA_IMAGEM</i>&gt; -f &lt;<i>ARQUIVO_DOCKERFILE</i>&gt; .
    </code>
    </pre>

3. Subido as imagens para o registry
    <pre>
    <code>
    for i in $(docker images | grep -i &lt;<i>REGISTRY</i>&gt; | awk '{print $1}' | uniq);do docker push -a $i;done
    </code>
    </pre>

## Usando as imagens

1. Crie o container informando a senha do usuário root:
    <pre>
    <code>
    docker run -it -d -e ROOT_PASS=&lt;<i>SENHA_DO_ROOT&gt;</i>&gt; --name &lt;<i>CONTAINER_NAME</i>&gt; &lt;<i>IMAGE_NAME</i>&gt;
    </code>
    </pre>

2. Acesse o container com SSH:
    <pre>
    <code>
    ssh root@$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' &lt;<i>CONTAINER_NAME</i>&gt;)
    </code>
    </pre>