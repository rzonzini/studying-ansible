#!/bin/sh

# Define a senha do root...
docker_set_root_password(){
  if [ -n "$ROOT_PASS" ]; then
    # ou para a senha passada como parâmetro do docker run -e ROOT_PASS
    echo "root:$ROOT_PASS" | chpasswd > /dev/null 2>&1
  else
    # ou uma gerada aleatoriamente
    ROOT_PASS=$(pwgen -1 32)
    echo "root:$ROOT_PASS" | chpasswd > /dev/null 2>&1
  fi
  echo "A senha do usuário root foi definida para $ROOT_PASS"
}

docker_ssh_service_start() {
  /usr/sbin/sshd -D
}

# check to see if this file is being run or sourced from another script
_is_sourced() {
  # https://unix.stackexchange.com/a/215279
  [ "${#FUNCNAME[@]}" -ge 2 ] \
    && [ "${FUNCNAME[0]}" = '_is_sourced' ] \
    && [ "${FUNCNAME[1]}" = 'source' ]
}

_main() {
  #exec "$@"
  docker_set_root_password
  docker_ssh_service_start
}

#if ! _is_sourced; then
  #_main "$@"
#fi

_main