# STRATEGY

## Linear

- Por padrão, o Ansible vai executar a tarefa em todos os *hosts* simultaneamente;
- A próxima tarefa só é iniciada quando a anterior foi completada em todos os *hosts*
- A quantidade de *hosts* em que a tarefa é executada simultaneamente é definida por ```forks``` no arquivo ```ansible.cfg```. O valor padrão é ```forks=5```
- ```Forks``` é a quantidade de *hosts* que o Ansible vai acessar simultaneamente
- O *playbook* só vai ser concluído quando todos os *hosts* concluirem as tasks

## Free

- Definido por ```strategy: free```
- Cada *host* executará as suas tarefas sem depender independentemente
- Por exemplo, se o *host 1* tiver mais recurso que o *host 2*, provavelmente, ele vai concluir o *playbook* antes do *host 2*

## Batch

- Define a quantidade de *hosts* que processarão o *playbook* com interdependencia
- A quantidade de *hosts* definida executará o *playbook* como na estratégia linear
- É definida pela opção ```serial: X```, onde ```X``` é a quantidade de *hosts*
- Permite definir um percentual de *hosts* do inventário (```serial: "30%"```)
- Permite definir um valor específico para cara *round* da execução, na forma de *array*
    <pre>
    <code>
    ---
    - name: test play
      hosts: webservers
      serial:
        - 1
        - 5
        - "20%"
    </code>
    </pre>

# REFERÊNCIA

1. [Controlling where tasks run: delegation and local actions](https://docs.ansible.com/ansible/latest/user_guide/playbooks_delegation.html#controlling-where-tasks-run-delegation-and-local-actions)
2. [Using keywords to control execution](https://docs.ansible.com/ansible/latest/user_guide/playbooks_strategies.html#using-keywords-to-control-execution)