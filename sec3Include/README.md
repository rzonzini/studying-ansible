# INVENTORY BEST PRACTICES

1. Separar o arquivo de inventário em arquivos YML com o nome do host
2. No arquivo de inventário principal permanece apenas o nome do host
3. O arquivo de inventário específico do host deve ser armazenado em um diretório, no mesmo nível do playbook, nomeado como ```host_vars```
4. De forma análoga, o arquivo de inventário de grupos de hosts deve ser armazenado no diretório, no mesmo nível do playbook, nomeado como ```group_vars```

## Exemple de estrutura de diretório

![diretório](../imagens/sec3-01.png)

## Exemple do conteúdo do arquivo de inventário principal
```
db_and_web_server
```

## Exemple do conteúdo do arquivo de inventário segregado
```
ansible_ssh_pass: Passw0rd
ansible_host: 192.168.1.14
```
# TASKS BEST PRATICES

1. Não usar todas as tarefas, com objetivos diferentes, no mesmo arquivo
2. Separar as tarefas em arquivos específicos, por afinidade
3. Usar a diretiva **include** no arquivo principal do playbook

## Exemplo de arquivo principal

![playbook.yml](../imagens/sec3-02.png)

## Exemplo de arquivos de tasks

![deploy_db.yml](../imagens/sec3-03.png)

![deploy_web.yml](../imagens/sec3-04.png)

# OBSERVAÇÕES

1. O host controlado precisa do python-apt: ```apt install -y python-apt```
2. Usando a imagem em [mmumshad/ubuntu-ssh-enabled](https://github.com/mmumshad/ubuntu-ssh-enabled)

# EXECUTAR

1. Iniciar os containers

    <pre>
    <code>
    docker run -d --name db_and_web_server2 -e ROOT_PASS=123qwe rzonzini/ubuntu-ssh-enabled:20.04
    </code>
    </pre>

    <pre>
    <code>
    docker run -d --name db_and_web_server2 -e ROOT_PASS=123qwe rzonzini/ubuntu-ssh-enabled:20.04
    </code>
    </pre>

2. Executar o playbook

    <pre>
    <code>
    ansible-playbook playbook.yml -i inventory.txt
    </code>
    </pre>

3. Para executar uma tarefas específica (que esteja com tag)

    ![Tags disponíveis](../imagens/sec3-05.png)

    <pre>
    <code>
    ansible-playbook playbook.yml -i inventory.txt --tags <i>tag</i>
    </code>
    </pre>

# REFERÊNCIA

1. [Ansible | Service Module](https://docs.ansible.com/ansible/2.5/modules/service_module.html)
