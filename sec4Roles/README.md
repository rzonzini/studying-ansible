# CONSIDERAÇÕES GERAIS
----------------------

1. O uso de **roles** permite reutilizar e compartilhar o código
2. ```ansible-galaxy init <NOME_DA_ROLE>``` criar a estrutura de diretórios necessária para a *role*
3. As tarefas da *role* são lidas do diretório *tasks*. Uma alternativa ao ```ansible-galaxy init <NOME_DA_ROLE>``` é criar a estrutura de diretórios (nome da role/tasks) e definir as *tasks* em um arquivo nomeado como *main.yml*.

# COMO FAZER
------------

O playbook definido na [Seção 3 - Include](../sec3FileSeparation/README.md), onde foi utilizado a diretiva **include** para segregar as tarefas necessários para a publicação do aplicação de forma monolítica, foi adaptado para a utilização de **roles** e publicação da aplicação em um ambiente distribuído.

Para apenas executar o **play**, execute os dois últimos passos.

1. Criar e acessar o diretório (roles) para agrupar as **roles** que serão utilizadas:
    <pre>
    <code>
    mkdir roles && cd roles
    </code>
    </pre>

2. Inicializar as *roles*:

   - **python** para agrupar as tarefas de instalação das dependencias;
   - **mysql_db** para agrupar as tarefas de instalação SGBD e configuração do banco de dados da aplicação; e,
   - **flask_web** para agrupar as tarefas da aplicação.

    <pre>
    <code>
    for i in python,mysql_db,flask_web;
    do
      ansible-galaxy init $i;
    done
    </code>
    </pre>

3. Copiar as tarefas abaixo do arquivo [playbook.yml](../sec3Include/playbook.yml) da Seção 3 - Include para o [main.yml](../sec4Roles/roles/python/tasks/main.yml) da role **python**:
   - Install all required dependencies
   - PyMySQL is required to

4. Copiar as tarefas abaixo do arquivo [deploy_db.yml](../sec3Include/tasks/deploy_db.yml) da Seção 3 - Include para o [main.yml](../sec4Roles/roles/mysql_db/tasks/main.yml) da role **mysql_db**:
   - Install MySQL database
   - Start MySQL Service
   - Create application database
   - Create database user

5. Incluir a *task* "Configure MySQL service" no [main.yml](roles/mysql_db/tasks/main.yml), antes de "Start MySQL Service", para que o serviço do MySQL escute na interface externa:

    <pre>
    <code>
    - name: Configure MySQL service
      shell:
        cmd: sed -i '/^bind-address/ s/127.0.0.1/0.0.0.0/' mysqld.cnf
        chdir: /etc/mysql/mysql.conf.d
      tags: 'db: configure-service'
    </code>
    </pre>

6. Definir, no arquivo [main.yml](roles/mysql_db/vars/main.yml) as variáveis de ambientes necessárias para a criação do banco de dados e do usário de banco da aplicação:

    <pre>
    <code>
    cat <<EOF >> roles/mysql_db/vars/main.yml 
    db_name: employee_db
    db_user: db_user
    db_user_password: Passw0rd
    EOF
    </code>
    </pre>

7. Copiar as tarefas abaixo do arquivo [deploy_web.yml](../sec3Include/tasks/deploy_web.yml) da Seção 3 - Include para o [main.yml](../sec4Roles/roles/flask_web/tasks/main.yml) da role **flask_main**:
   - Install Python Flask dependency
   - Copy source code
   - Start web server

8. Definir, no arquivo [main.yml](roles/flask_web/vars/main.yml) a variável de ambientes necessária para a definir o host do banco de dados onde a aplicação deve se conectar:

    <pre>
    <code>
    cat <<EOF >> roles/flask_web/vars/main.yml 
    mysql_database_host: 172.17.0.2
    EOF
    </code>
    </pre>

9. Alterar a *task* "Start web server" para exportar a variável de ambiente do SO com a definição do host do banco de dados.

10. Criar o arquivo [playbook.yml](playbook.yml) contendo as *plays* **Deploy a mysql DB** e **Deploy a web server**:

    <pre>
    <code>
    cat <<EOF > playbook.yml
    - name: Deploy a mysql DB
      hosts: db_server
      roles:
      - python
      - mysql_db
    
    - name: Deploy a web server
      hosts: web_server
      roles:
      - python
      - flask_web
    EOF
    </code>
    </pre>

11. Criar o arquivo [inventory.txt](inventory.txt) contendo os grupos **db_server** e **web_server** e as credenciais dos hosts:

    <pre>
    <code>
    cat <<EOF > inventory.txt
    db_server ansible_host=172.17.0.2 ansible_ssh_user=root ansible_ssh_pass=123qwe
    
    web_server ansible_host=172.17.0.3 ansible_ssh_user=root ansible_ssh_pass=123qwe
    EOF
    </code>
    </pre>

12. Iniciar os containers:

    <pre>
    <code>
    for i in target1 target2;
    do
      docker run -it -d -e ROOT_PASS=123qwe --rm --name $i rzonzini/ubuntu-ssh-enabled:latest;
    done
    </code>
    </pre>

13. Executar o [playbook.yml](playbook.yml)

    <pre>
    <code>
    ansible-playbook playbook.yml -i inventory.txt
    </code>
    </pre>

# REFERÊNCIA
------------

[Roles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html?highlight=roles#roles)

# TROUBLESHOOTING
-----------------
## SSH fingerprint

### Mengagem de erro

  {"msg": "Using a SSH password instead of a key is not possible because Host Key checking is enabled and sshpass does not support this.  Please add this host's fingerprint to your known_hosts file to manage this host."}

### Solução

```export ANSIBLE_HOST_KEY_CHECKING=False```