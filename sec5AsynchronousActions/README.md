# ASYNCHRONOUS ACTIONS

- Run a process and check on it later
- Run multiple processes at once and check on them later
- Run processes and forget

## Async and Poll

- **async** - How long to run?

  time amount waiting for the task result

- **poll** - How frequently to check? (default 10 seconds)

  time amount to start the next task. ```poll: 0``` does't wait to start the execution of the next task

- **async_status** - Check status of an async task

## Exemple

<pre>
<code>
# Ansible Playbook

- name: Deploy Web Application
  hosts: db_and_web_server
  tasks:
    - command: /opt/monitor_webapp.py
      async: 360
      poll: 0
      register: webapp_result

    - command: /opt/monitor_database.py
      async: 360
      poll: 0
      register: database_result

    - name: Check status of tasks
      async_status: jid={{ webapp_result.ansible_job_id }}
      register: job_result
      until: job_result.finished
      retries: 30
</code>
</pre>

# REFERÊNCIA

1. [Asynchronous actions and polling](https://docs.ansible.com/ansible/latest/user_guide/playbooks_async.html#asynchronous-actions-and-polling)